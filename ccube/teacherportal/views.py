from django.shortcuts import render, HttpResponse
from home.models import Contact
from django.contrib import messages
from .forms import QuizForm, JoinForm
from teacherportal.models import JoinUs


def home(request):
    # return HttpResponse("This is homepage!")
    return render(request, 'teacher/home.html')


def createquiz(request):
    if request.method == 'POST':
        filled_form = QuizForm(request.POST, request.FILES)
        if filled_form.is_valid():
            filled_form.save()
            note = 'Your Question Regarding Has Been Added to the database, Keep Adding More'
            newform = QuizForm()
            return render(request, "teacher/createquiz.html", {'quizform': newform, 'note': note})
    else:
        form = QuizForm()
        return render(request, "teacher/createquiz.html", {'quizform': form})


def joinus(request):
    if request.method == 'POST':
        filled_form = JoinForm(request.POST, request.FILES)
        if filled_form.is_valid():
            filled_form.save()
            note = messages.success(
                request, "Your message has been successfully sent")

            newform = JoinForm()
            return render(request, "teacher/joinus.html", {'joinform': newform, 'note': note})
    else:
        form = JoinForm()
        return render(request, "teacher/joinus.html", {'joinform': form})


# def joinus(request):
#     # return HttpResponse("This is cntact page!")
#     if request.method == "POST":
#         bio = request.POST['bio']
#         email = request.POST['email']
#         experience = request.POST['experience']
#         expertise = request.POST['expertise']
#         image = request.POST['image']
#         if  len(email) < 3 or len(bio) < 4:
#             messages.error(request, "Please fill the form correctly!")
#         else:
#             join = JoinUs(bio=bio, email=email,experience=experience, expertise=expertise,image=image)
#             join.save()
#             messages.success(
#                 request, "Your message has been successfully sent")
#     return render(request, "teacher/joinus.html")


def signup(request):
    if request.method == "POST":
        # Get the post parameters
        username = request.POST['username']
        email = request.POST['email']
        # fname=request.POST['fname']
        # lname=request.POST['lname']
        pass1 = request.POST['pass1']
        pass2 = request.POST['pass2']

        # check for errorneous input
        if len(username) > 10:
            messages.error(
                request, " Your user name must be under 10 characters")
            return redirect('home')

        if not username.isalnum():
            messages.error(
                request, " User name should only contain letters and numbers")
            return redirect('home')
        if (pass1 != pass2):
            messages.error(request, " Passwords do not match")
            return redirect('home')

        # Create the user
        myuser = User.objects.create_user(username, email, pass1)
        # myuser.first_name= fname
        # myuser.last_name= lname
        myuser.save()
        messages.success(request, " Your iCoder has been successfully created")
        return redirect('home')
