# Generated by Django 3.1.3 on 2020-11-30 06:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teacherportal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='quiz',
            name='email',
            field=models.EmailField(default='', max_length=254),
        ),
        migrations.AddField(
            model_name='quiz',
            name='image',
            field=models.ImageField(default='', upload_to=''),
        ),
        migrations.AddField(
            model_name='quiz',
            name='subject',
            field=models.CharField(default=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='quiz',
            name='question',
            field=models.CharField(max_length=2555),
        ),
    ]
