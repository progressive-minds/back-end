from django.db import models

# Create your models here.


class Quiz(models.Model):
    question = models.CharField(max_length=2555)
    option1 = models.CharField(max_length=30)
    option2 = models.CharField(max_length=30)
    option3 = models.CharField(max_length=30)
    option4 = models.CharField(max_length=30)
    answer = models.CharField(max_length=30)
    image = models.ImageField(max_length=100, blank=False, default="")
    subject = models.CharField(max_length=50, default=True)
    difficulty = models.CharField(max_length=50, default=True)
    email = models.EmailField(max_length=254, default="")



class JoinUs(models.Model):
    sno = models.AutoField(primary_key = True)
    bio = models.TextField()
    email = models.CharField( max_length=100)
    experience = models.CharField(max_length=50, default=True)
    expertise = models.CharField(max_length=50, default=True)
    image = models.ImageField(max_length=100, blank=False, default="")
    timeStamp = models.DateTimeField( auto_now_add=True, blank = True)

    def __str__(self):
        return 'Message from ' + self.email



