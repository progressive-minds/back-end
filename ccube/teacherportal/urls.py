from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('createquiz', views.createquiz, name='createquiz'),
    path('joinus',views.joinus, name='joinus'),
    path('signup/', views.signup, name='signup'),
]
