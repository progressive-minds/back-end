from django.contrib import admin
from .models import Quiz, JoinUs
# Register your models here.
admin.site.register(Quiz)

admin.site.register(JoinUs)
