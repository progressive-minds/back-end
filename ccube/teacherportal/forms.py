from django import forms
from .models import Quiz, JoinUs
# class QuizForm(forms.Form):
#     topic = forms.CharField(label="Topic",max_length=255)
#     question = forms.CharField(label="Question",max_length=255)
#     option1 = forms.CharField(label="Option 1",max_length=30)
#     option2 = forms.CharField(label="Option 2",max_length=30)
#     option3 = forms.CharField(label="Option 3",max_length=30)
#     option4 = forms.CharField(label="Option 4",max_length=30)
#     correctasnwer = forms.CharField(label="Correct Answer",max_length=30)


class QuizForm(forms.ModelForm):
    image = forms.ImageField()
    email = forms.EmailField()
    subject = forms.ChoiceField(label='subject', choices=[('Maths', 'Maths'), (
        'Physics', 'Physics'), ('Chemistry', 'Chemistry'), ('Biology', 'Biology')])
    difficulty = forms.ChoiceField(label='difficulty', choices=[('Easy', 'Easy'), (
        'Medium', 'Medium'), ('Hard', 'Hard')])
    # name = forms.ModelChoiceField(queryset=Subject.objects)

    class Meta:
        model = Quiz
        fields = [
            'question',
            'option1',
            'option2',
            'option3',
            'option4',
            'answer',
            'subject',
            'difficulty',
            'image',

            'email',
        ]
        labels = {
            'question': 'Question',
            'option1': 'Option 1',
            'option2': 'Option 2',
            'option3': 'Option 3',
            'option4': 'Option 4',
            'answer': 'Answer',

            'subject': 'subject',
            'difficulty': 'difficulty',
            'image': 'image',
            'email': 'email',

        }
        # widgets = {
        #     'question':forms.Textarea
        # }

class JoinForm(forms.ModelForm):
    image = forms.ImageField()
    email = forms.EmailField()
    expertise = forms.ChoiceField(label='subject', choices=[('Maths', 'Maths'), (
        'Physics', 'Physics'), ('Chemistry', 'Chemistry'), ('Biology', 'Biology')])
    experience = forms.ChoiceField(label='expertise', choices=[('1-2months', '1-2 months'), (
        '1 yr', '1 yr'), ('More than 1 yyr', 'More than 1 yrr')])
       
    # name = forms.ModelChoiceField(queryset=Subject.objects)

    class Meta:
        model = JoinUs
        fields = [
            'expertise',
            'email',
            'bio',
            'experience',
            'image'
        ]
        labels = {
            'bio':'bio',
            'experience': 'experience',
            'expertise': 'expertise',
            'image': 'image',
            'email': 'email',

        }